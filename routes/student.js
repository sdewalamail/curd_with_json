const express = require('express');
const route = express.Router();
const studentModel = require('../model/student')


 route.get("/", async (req,res) =>{

    console.log('ok');
      const data = await studentModel.find();
      res.json(data);
       
 });



 route.post("/", async (req,res) =>{

    console.log('ok');
      const data = await studentModel.create(req.body);
       await data.save()
      res.json(req.body);
       
 });


  route.delete('/:id',async (req, res) => {
    
      const id = req.params.id;
    console.log(id);
      const delStudent =  await studentModel.findByIdAndDelete({_id:id});

       res.json(delStudent);
  });


  //////////////////// update///////////////////

  route.put('/:id',async (req, res) => {
    
    const id = req.params.id;
     const params = req.body;
   console.log(params);

   
     const updateStudent =  await studentModel.findByIdAndUpdate({_id:id},{$set:params});

     res.json({ status: 'success', error_code: 0, result: updateStudent, message: 'Article updated successfully' });
});

module.exports = route;