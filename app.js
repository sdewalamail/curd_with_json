'use strict';
const express = require('express');
const  db  = require('./config/db');
const app = express();


 require('dotenv')
const port = process.env.PORT || 8000;



app.use(express.json());
app.use(express.urlencoded())
app.use('/', require('./routes/index'));



db().then(e => app.listen(port, () => {

     console.log(`port is listening ${port}`);
}) ).catch(e =>{

     console.log("unable to connect the database");
      process.exit(1)
}) 
